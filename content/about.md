---
title: "About"
layout: "about"
url: "/about/"
summary: about
---

# Hello! This is Darío talking!

I'm Darío Ferrer, a software developer eager to experiment in different projects.

Data Science and Machine Learning are probably the fields I'm mostly interested into, so I came up with the idea of creating this blog, that keeps track of all projects/analysis done about this topics, always applied to football.   