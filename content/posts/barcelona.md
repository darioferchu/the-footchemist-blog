---
title: 'Analysis on FC Barcelona performance decrease'
date: '2024-01-29T16:22:50+01:00'
draft: false
cover:
    image: images/barcelona.jpg
    alt: 'FC Barcelona'
    caption: 'FC Barcelona'
    relativeSize: "50x50"
---

## What is happening to FC Barcelona this season? Why the low performance?

In this post we will be addressing the decrease in performance that FC Barcelona has experimented this season relative to the past year, in which they managed to rise from their ashes to accomplish a League Title and making everyone believe they are again a competitive team that aimed for the highest objectives.

In order to perform this analysis, FBRef data is going to be used:
- Passing metrics
- Shooting metrics
- Possession metrics
- Defensive metrics 

## Preprocessing pipeline
Metrics from 2022 and 2023 seasons are fetched. Then, they are normalized using the number of matches played at the moment. This way metrics by 90 minutes are obtained, allowing the direct comparison in performance.

Resulting plots are displayed for each type of metric.

## Passing data
Passing data comprehends metrics that are related to passing performance of teams. This metrics proportionate an idea of the playing style of a team, and accomplishment of it.

![Markdown Logo](/images/barcelona/bars_passing.png)
*Passing metrics, with percentage differences*

As can be seen in the plot, there are not substantial differences in Pass Completion metrics. Where significant differences can be seen is in metrics that are related to performance near the opponent box, and subsequently, the diminishing of the performance. The most perceivable differences in metrics to take into account are the following:

### Lower efficiency at converting chances
This can be infered from **Assists - xAG** metric. This indicates the difference between assists and expected assisted goals. 
There is a significant decrease of 29 % in 2023. As can be seen, **Assists** are lower, which means that fewer goals have been converted despite the quality of the assists given. This could be due to worse finishing efficiency of the strikers.
### Less passing in chance prone areas of the pitch

**Passes into final 1/3**,**Passes into penalty area**,**Crosses into penalty area** and **Progressive passes** have dropped significantly, more than 10% in almost all cases. This means that the team is not playing that high on the pitch or attackers are being less prominent at providing passes to that last part of the pitch in which there is more chance to shoot and create opportunities.

### Passing Radar
![Markdown Logo](/images/barcelona/radarPassing.png)
*Passing radar*
## Shooting data
Shooting performance is crucial for the success of a team. In these case several metrics are compared between this and past year for FC Barcelona.
![Markdown Logo](/images/barcelona/bars_shooting.png)
*Shooting metrics, with percentage differences*

### More shots, less quality
As can be infered from **Shot on target %**,**Shots per 90 mins**,**Shots on target per 90 mins** metrics, the team shot more and more specifically on target this season. This doesn't mean the team improved its performance, in fact, when comparing **Goals per shot on target**, there is a 13% decrease, meaning that despite the shots are on target, they are not more dangerous for the keepers.

This can be reaffirmed taking into account the 7% decrease in **non penalty xG per shot** data. There is more accumulated xG because there are more shots overall, but the shots accumulate less xG each, meaning they are not that dangerous to end un being a scored goal.

### Shooting Radar
![Markdown Logo](/images/barcelona/radarShooting.png)
*Shooting radar*
## Possession data
Possession metrics are important, even more in this case, because FC Barcelona is known to play a possession centered style of football. By revising the comparison between both seasons, it may be possible to spot relevant differences that explain the decrease of performance.
![Markdown Logo](/images/barcelona/bars_possession.png)
*Possession metrics, with percentage differences*

### More deffensive play, less final third presence
**Touches in Defensive Third** have increased, and **Touches in Attacking Third** have decreased. This is indicative of how the team plays more in its own field and has less pressence in attacking third. 

This is endorsed by the decrease in 
touches in penalty area, succesful take ons, carries into final third, carries into penalty area and above all, **Progressive Receptions**, that dropped 13%. This indicates that Barcelona had much trouble this season into filtering and receiving balls that progressed in the last third of the field.
 
### Possession Radar
![Markdown Logo](/images/barcelona/radarPossession.png)
*Possession radar*

## Defensive data
In the last batch of metrics to check, defensive data is taken into account. This metrics can provide an insight of defensive performance of the team, and if it has affected the results of it.
![Markdown Logo](/images/barcelona/bars_defense.png)
*Defensive metrics, with percentage differences*

### Team is deffending closer to its goal
The deffending aspect is probably where can be seen more clearly the difference of the team from one year to another.
**Tackles in Defensive Third** have increased 37% and **Tackles in Attacking Third** have decreased by 24%. This could mean the pressure is not working as good as the past year, and the team has to defend closer to its goal.

It is also revealing the **Clearances** metric, 40% increase, the team is suffering more in defense and has to clear the ball much more than the past year.

### Defensive Radar
![Markdown Logo](/images/barcelona/radarDefensive.png)
*Defensive radar*

## Discussion
No strong statistical claims can be infered from this analysis, but is useful to check the differences between the metrics that are responsible for the performance of teams.
As could be seen in each respective section, there have been several metrics that are not as high as the past year, which might be leading to the decrease in performance.

In a team that should be dominating with the ball, it is concerning that the game surrounding the oposition box and inside it has decreased. The team seems to be defending in a lower block, due to the decrease in pressure performance, and that leads to more cleareances and less interceptions at high stages of the field. This, added to the effectivity decrease, might be responsible for the decrease in performance.




