---
title: 'Clustering analysis of 5 big leagues'
date: '2024-01-04T19:22:50+01:00'
draft: false
cover:
    image: images/kmeans_5.png
    alt: 'Clustering'
    caption: 'Clustering'
    relativeSize: "50x50"
---

## Is it possible to categorize the styles of play in Europe?

In this post I will be trying to analyze the styles of play in Europe and how different teams can be related in
playing terms. The idea is to make a clustering analysis on the teams, based on different parameters scrapped from several stat websites.

Teams from Bundesliga, La liga, Premier League, Serie A and French League are selected, as well as a set of features that could be relevant in order to approach the analysis.

## Data retrieval
The project relies on the soccerdata API to retrieve team statistics.

- **Passing statistics**: passing is a fundamental aspect of soccer, and examining passing statistics provides valuable insights into team strategy and coordination. The soccerdata API facilitates the extraction of detailed passing metrics for teams in the Big 5 European Leagues Combined.

- **Shooting statistics**: the prowess of a soccer team in the offensive phase is encapsulated through shooting statistics. This section explores the shooting metrics retrieved from the API, showing valuable information of teams' goal-scoring capabilities and shooting accuracy.

- **Possession statistics**: possession statistics play a pivotal role in understanding a team's control over the game. By analyzing possession data, we aim to uncover trends in ball dominance and the ability to dictate the flow of play.


- **Defense statistics**: defense is an essential part of a successful soccer team. The way a team defends is an indicator of the true level a soccer team has. Defense stats undergo preprocessing for uniformity in column names.

## Preprocessing

- **Merging**: to facilitate an analysis of the performance of the teams, individual dataframes for passing, shooting, possession, and defense are merged into a unified dataset
- **Missing values**: to address missing values, a SimpleImputer is employed, replacing NaN entries with the mean of the respective columns. This ensures a solid dataset for subsequent analyses.
- **Normalization**: normalization is crucial for consistent scaling, especially when dealing with diverse numerical columns, including percentages. The adoption of MinMax normalization guarantees a level playing field for all features.

## Dimensionality reduction

PCA is applied for reducing the dimensionality of data. A variance analysis is conducted to determine the number of principal components required to capture a substantial proportion of the dataset's variability.

The top 10 principal components are extracted, each representing a linear combination of the original features. These components, ranked by their contribution to variance, form the basis of a condensed feature space.

![Markdown Logo](/images/varianceCovered.png)
*Variance explained by eigenvectors.*

For the sake of visibility and interpretability, only two principal componentes are taken, even though the variance explained is just 60%. For a classification problem, more components should be used in order to explain at least 80~85% of variance.

## Relevant metrics

Only two first components are going to be used for representation and analysis purposes. The contribution of each feature in first and second component are represented.

### First component
The ten most important features that impact the first component are:

- **xG**: expected goals
- **npxG**: non-penalty expected goals
- **xAG**: expected assisted goals
- **PrgP**: progressive passes
- **PrgR**: progressive received passes
- **Poss**: possession
- **PPA**: passes into penalty area
- **TotalAttack**: total attacks
- **Touches**: number of times a player touched the ball
- **Touches Live**: live ball touches, that excludes corner kicks, penalties, free kicks and such plays.

![Markdown Logo](/images/variances_feature_1_10_max.png)
*First component.*

### Second component
The ten most important features that impact the first component are:

- **A-xAG**: assists minus expected goals assisted.
- **G/Sh**: goals per shot.
- **G/SoT**: goals per shot on target.
- **npG-xG**: non-penalty goals minus expected goals.
- **G-xG**: goals minus expected goals.
- **Ast**: assists.
- **Gls**: goals.
- **xA**: expected assists.
- **SoT%**: shots on target(percentage).
- **xG/Sh**: expected goal per shot.

![Markdown Logo](/images/variances_feature_2_10_max.png)
*Second component.*

## K-Means
K-Means algorithm is applied to generate the clustering. 

### Inertias

In order to select most optimal number of clusters,inertias are plotted and elbow rule is used by visually inspecting the plot.
![Markdown Logo](/images/inertias.png)
*Inertias.*

The chosen number of clusters could be 6 or 7. From this number,the inertia decrease is lower.
### Cluster representation

Each data point on the plot represents a soccer team,  98 being the total number of teams. The data points are colored according to their assigned clusters.The axes of the scatter plot correspond to the principal components resulting from the PCA.

![Markdown Logo](/images/kmeans_6.png)
*K-Means with 6 clusters.*



## Discussion
From the plot we can see that the German teams are put high in the y-axis, which is due to the fact that they tend to defend very high in the pitch, leading to almost no tackles in the defensive third. They also do not pass much. 

It is a vertical style in a league that scores more goals than the other four leagues. They are very effective teams in terms of goal scoring. The German teams that have the best players, such as Leipzig, Dortmund and Bayern Munich are further to the right because they progress more with the ball combined with defense very high on the pitch and steals in the opponent’s side. So, specifically, Bayern Munich combines the best parts from both German style of defense and a more developed style of passing and combinations.

Manchester City was clearly the best team by far in Europe. Last year they went to win the Premier League and the Champions League in arguably the best season in their history, both in terms of style and trophies.

Other elite clubs such as Barcelona, Real Madrid or PSG were also creating a lot of chances and progressing with the ball, but were not defending, stealing and pressing as good as Manchester City did, hence they are lower in the y-axis. 

Then the left-most teams such as Cadiz, Ajaccio and those teams in that group show signs of lack of possession, passes and attacking capabilities along with a poor and low on the pitch defense, which leads to a great number of tackles in the defensive third and defending most of the matches played. These teams’ style is commonly referred to as “parking the bus”. This is an ultra-defensive style of football where teams do anything to preserve their clean sheet. By putting all 11 players behind the ball to stop the opponent’s attacking threat, and then rarely attacking yourself is considered this style of playing.




